# Copyright 2019-2021 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit acct-user

ACCT_USER_ID=-1
ACCT_USER_HOME=/var/lib/gitlab-runner
ACCT_USER_GROUPS=( gitlab-runner )

acct-user_add_deps
