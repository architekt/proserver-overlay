# Copyright 2019-2022 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

YQ_VER=3.4.1
CONFIG_VER=90
KERNEL_VER=5.15.26

DESCRIPTION="Standard implementation of lightweight Virtual Machines containers with workload isolation and security advantages of VMs"
HOMEPAGE="https://github.com/kata-containers/kata-containers"
SRC_URI="https://github.com/${PN}/${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz
	 https://github.com/mikefarah/yq/releases/download/${YQ_VER}/yq_linux_amd64
	 https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-${KERNEL_VER}.tar.gz
"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64"
#IUSE="agent kernel runtime"

RDEPEND="
	=dev-lang/go-1.17.12
	virtual/rust
"
DEPEND="${RDEPEND}"

PATCHES=( "${FILESDIR}"/config-cargo.patch )

src_unpack() {
	default
	unpack "${FILESDIR}"/"${PN}"-agent-vendor-"${PV}".tar.gz
}

src_prepare() {
	mkdir "${S}"/bin

	cp "${DISTDIR}"/yq_linux_amd64 "${S}"/bin/yq
	chmod +x "${S}"/bin/yq

	cp -r "${WORKDIR}"/vendor "${S}"/src/agent

	default
}

src_configure() {
	local PATH="$PATH:${S}/bin"

        pushd tools/packaging/kernel
          ./build-kernel.sh -a x86_64 -f -k "${WORKDIR}"/linux-"${KERNEL_VER}" setup
        popd
}

src_compile() {
	local PATH="$PATH:${S}/bin"

	pushd src
	  GOPATH="${S}" \
		emake -C runtime \
		PREFIX="${EPREFIX}/usr" \
		LIBDIR="${EPREFIX}/usr/$(get_libdir)"

	  LIBC="gnu" \
		emake -C agent \
		PREFIX="${EPREFIX}/usr"
        popd

	pushd tools/packaging/kernel
	  ./build-kernel.sh -a x86_64 -f -k ${WORKDIR}/linux-${KERNEL_VER} build
	popd
}

src_install() {
	local PATH="$PATH:${S}/bin"

	pushd src
	  GOPATH="${S}" \
		emake -C runtime \
		PREFIX="${EPREFIX}/usr" \
		LIBDIR="${EPREFIX}/usr/$(get_libdir)" \
		SYSCONFDIR="${EPREFIX}/etc" \
		DESTDIR="${D}" install

	  LIBC="gnu" \
		emake -C agent \
		BINDIR="/usr/bin" \
		DESTDIR="${D}/usr/$(get_libdir)/libexec" install
	popd

        pushd tools/packaging/kernel
          DESTDIR="${D}" ./build-kernel.sh -a x86_64 -f -k ${WORKDIR}/linux-${KERNEL_VER} install
        popd
}
