# Copyright 2019-2022 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

YQ_VER=3.4.1

DESCRIPTION="Standard implementation of lightweight Virtual Machines containers with workload isolation and security advantages of VMs"
HOMEPAGE="https://github.com/kata-containers/kata-containers"
SRC_URI="https://github.com/${PN}/${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz
	 https://github.com/mikefarah/yq/releases/download/${YQ_VER}/yq_linux_amd64"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="
	=dev-lang/go-1.17.12
	virtual/rust
"
DEPEND="${RDEPEND}"

src_prepare() {
        mkdir "${S}"/bin 
	cp "${DISTDIR}"/yq_linux_amd64 "${S}"/bin/yq
        chmod +x "${S}"/bin/yq
	default
}

src_compile() {
	pushd src
	GOPATH="${S}" \
	emake -C runtime \
		PREFIX="${EPREFIX}/usr" \
		LIBDIR="${EPREFIX}/usr/$(get_libdir)"
        popd src
}

src_install() {
	pushd src
	GOPATH="${S}" \
	emake -C runtime \
		PREFIX="${EPREFIX}/usr" \
		LIBDIR="${EPREFIX}/usr/$(get_libdir)" \
		SYSCONFDIR="${EPREFIX}/etc" \
		DESTDIR="${D}" install
	popd src
}
