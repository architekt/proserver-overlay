# Copyright 2019-2021 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit go-module

DESCRIPTION="GitLab runner for CI/CD jobs"
HOMEPAGE="https://gitlab.com/gitlab-org/gitlab-runner"

SRC_URI="https://gitlab.com/gitlab-org/${PN}/-/archive/v${PV}/${PN}-v${PV}.tar.gz -> ${P}.tar.gz"
LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE="docker"
MY_REV=2ebc4dc4

RDEPEND="
    docker? ( app-emulation/docker )
    acct-user/gitlab-runner
    acct-group/gitlab-runner"

DOCS=( {CHANGELOG,README}.md )

S="${WORKDIR}/${PN}-v${PV}"

src_compile() {
    local LDFLAGS="
        -X gitlab.com/gitlab-org/gitlab-runner/common.NAME=${PN}
        -X gitlab.com/gitlab-org/gitlab-runner/common.VERSION=${PV}
        -X gitlab.com/gitlab-org/gitlab-runner/common.REVISION=${MY_REV}
        -X gitlab.com/gitlab-org/gitlab-runner/common.BUILT=$(date -u +%Y-%m-%dT%H:%M:%S%z)
        -X gitlab.com/gitlab-org/gitlab-runner/common.BRANCH=master"

    GOFLAGS="-v -x -mod=vendor" \
    go build -ldflags "$LDFLAGS" -o bin/gitlab-runner || die "go build failed"
}

src_test() {
    GOFLAGS="-v -x -mod=vendor" go test -work || die "test failed"
}

src_install() {
    einstalldocs

    exeinto /usr/libexec/gitlab-runner
    doexe bin/gitlab-runner
    dosym ../libexec/gitlab-runner/gitlab-runner /usr/bin/gitlab-runner
}

pkg_postinst() {
    go-module_pkg_postinst

    elog ""
    elog ""
    elog "WARNING:"
    elog "Register the runner using"
    elog "# gitlab-runner register"
    elog ""
    elog "This will save the config in"
    elog "/etc/gitlab-runner/config.toml"
    elog ""
    elog ""
}
