# Copyright 2019-2021 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Bash script to create nftables sets of country specific IP address ranges for use with firewall rulesets"
HOMEPAGE="https://github.com/wirefalls/geo-nft/"
SRC_URI="https://github.com/wirefalls/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~arm ~x86"

RDEPEND="
    net-firewall/nftables
    sys-apps/gawk
    net-misc/curl
    sys-apps/grep
    sys-apps/sed
    sys-apps/coreutils"

src_prepare() {
    eapply_user
    sed -i -e 's|geo_conf="/etc/geo-nft.conf"|geo_conf="/etc/geo-nft/geo-nft.conf"|' geo-nft.sh || die
    sed -i -e 's|errorlog="/etc/geo-nft-error.log"|errorlog="/var/log/geo-nft-error.log"|' geo-nft.sh || die
}

src_install() {
    keepdir /etc/${PN}
    exeinto /etc/nftables/${PN}
    exeopts -m 0744
    doexe ${PN}.sh
    dosym /etc/nftables/${PN}/${PN}.sh /usr/bin/${PN}

    docinto examples
    dodoc "${FILESDIR}"/ipv4.example
    dodoc "${FILESDIR}"/ipv6.example
    dodoc "${FILESDIR}"/ipv46.example
}
